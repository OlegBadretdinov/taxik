//: Playground - noun: a place where people can play

import UIKit

// MARK: Задание 1.1
func task1(e :Double, x:Double) -> Double {
    var i = 1
    var z :Double = 1
    
    var factorial = 1
    
    while (true) {
        factorial *= (2 * i) * (2 * i - 1)
        
        let delta = pow(x, Double(2 * i)) / Double(factorial)
        z += delta
        
        if delta < e {
            break
        }
        
        i += 1
    }
    
    return z
}

// MARK: Задание 1.2
func task2(k :Double, l :Double, x:Double) -> (t :Double, q:Double) {
    let t = pow(cos(x), 2)*(pow(k, 2) - pow(l, 2))/(k * l * x)
    let q = sqrt( (pow(t,2) * abs(k - l)) / 0.25 )
    
    return (t, q)
}

// MARK: Задание 2.1
func task3(numbers :[Double]) ->  [Double] {
    assert(numbers.count > 0)
    
    var outputArray = [Double]()
    let minX = numbers.minElement()!
    
    for x in numbers {
        let y = pow(x, 3) / minX
        
        outputArray.append(y)
    }
    
    return outputArray
}

// MARK: Задание 2.2
func task4(a :[Int], b:[Int]) -> [Int] {
    var outArray = a.filter() { $0 < 0 }
    outArray.appendContentsOf(b.filter({ (value) -> Bool in
        value < 0
    }))
    return outArray
}

task1(0.001, x: 1)
task2(3, l: 1, x: M_PI)
task3([-1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
task4([-1, -2, -3, -4, 5, 6, 7, 8, -9, -10, 11, 12, -13, 14, -15], b: [111, -222, 333, 444, -555])

//
//  TXCitiesViewModel.swift
//  Taxik
//
//  Created by Oleg Badretdinov on 22.05.16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit
import TaxikApi
import ReactiveCocoa

class TXCitiesViewModelState:NSObject {
    private(set) var cities :[TXCity]
    
    init(cities: [TXCity]) {
        self.cities = cities
    }
}

class TXCitiesViewModel: NSObject {
    private let networkModel :TXNetworkModel
    private(set) var isLoading :MutableProperty<Bool> = MutableProperty<Bool>(false)
    private(set) var isErrorViewHidden: MutableProperty<Bool> = MutableProperty<Bool>(false)
    
    private(set) var state :MutableProperty<TXCitiesViewModelState> = MutableProperty<TXCitiesViewModelState>(TXCitiesViewModelState(cities: []))
    
    init(networkModel :TXNetworkModel) {
        self.networkModel = networkModel
    }
    
    func reloadCities()  {
        self.isLoading.value = true
        self.isErrorViewHidden.value = true
        
        self.networkModel.getMapPoints().subscribeNext({ [weak self] (object) in
            guard let strongSelf = self else {
                return
            }
            
            let response = object as! TXCitiesResponse
            if response.cities != nil {
                strongSelf.state.value = TXCitiesViewModelState(cities: response.cities)
            } else {
                strongSelf.state.value = TXCitiesViewModelState(cities: [])
            }
            }, error: { [weak self] (error) in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.state.value = TXCitiesViewModelState(cities: [])
                
                strongSelf.isLoading.value = false
                strongSelf.isErrorViewHidden.value = false
        }) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.isErrorViewHidden.value = true
            strongSelf.isLoading.value = false
        }
    }
}

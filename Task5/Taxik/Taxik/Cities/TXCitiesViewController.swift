//
//  TXCitiesViewController.swift
//  Taxik
//
//  Created by Oleg Badretdinov on 22.05.16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit
import ReactiveCocoa
import Typhoon
import TaxikApi
import MapKit

private let reuseIdentifier = "CityCollectionViewCell"

class TXCitiesViewController: UIViewController {

    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    private var viewModel: TXCitiesViewModel!
    private var selectedCityIndex :NSIndexPath!
    
    private var annotation: MKPointAnnotation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViewModel()
        self.setupCommands()
        self.setupObservers()
        
        self.reloadCities()
        
        let region = self.mapView.regionThatFits(MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(55.743290, 37.620111), 10000, 10000))
        self.mapView.setRegion(region, animated: true)
    }
    
    private func setupViewModel() {
        self.viewModel = TyphoonComponentFactory.defaultFactory().componentForKey("txCitiesViewModel") as! TXCitiesViewModel
    }
    
    private func setupCommands() {
        self.refreshButton.rac_command = RACCommand(signalBlock: {[weak self] (input) -> RACSignal! in
            self?.reloadCities()
            
            return RACSignal.empty()
        })
    }
    
    private func setupObservers() {
        self.segmentedControl.rac_newSelectedSegmentIndexChannelWithNilValue(0).subscribeNext { [weak self] (object) in
            guard let strongSelf = self else {
                return
            }
            
            switch strongSelf.segmentedControl.selectedSegmentIndex {
            case 0:
                strongSelf.mapView.mapType = MKMapType.Standard
            case 1:
                strongSelf.mapView.mapType = MKMapType.Satellite
            case 2:
                strongSelf.mapView.mapType = MKMapType.Hybrid
            default:
                break
            }
        }
        
        self.errorView.rac_hidden <~ self.viewModel.isErrorViewHidden
        self.mapView.rac_hidden <~ self.viewModel.isLoading
        self.segmentedControl.rac_hidden <~ self.viewModel.isLoading
        self.collectionView.rac_hidden <~ self.viewModel.isLoading
        
        self.activityIndicator.rac_hidden <~ self.viewModel.isLoading.signal.map { (isLoading) -> Bool in
            return !isLoading
        }
        
        self.viewModel.state.signal.observeNext { [weak self] (state) in
            self?.collectionView.reloadData()
        }
    }
    
    private func reloadCities() {
        self.viewModel.reloadCities()
    }
    
    
}

extension TXCitiesViewController :UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.state.value.cities.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! TXCityCollectionViewCell
        
        let city = self.viewModel.state.value.cities[indexPath.item]
        cell.nameLabel.text = city.cityName
        
        if let location = city.location {
            cell.coordinatesLabel.text = "lat = \(location.coordinate.latitude); lon = \(location.coordinate.longitude)"
        }
        
        if let selectedCityIndex = self.selectedCityIndex {
            if selectedCityIndex == indexPath {
                cell.backgroundColor = UIColor(red: 253.0/255.0, green: 219.0/255.0, blue: 158.0/255.0, alpha: 1)
            }
        }
        
        return cell
    }
}

extension TXCitiesViewController :UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let city = self.viewModel.state.value.cities[indexPath.item]
        self.placeCityPin(city)
        
        let oldIndexOpt = self.selectedCityIndex
        self.selectedCityIndex = indexPath
        if let oldIndex = oldIndexOpt {
            self.collectionView.reloadItemsAtIndexPaths([indexPath, oldIndex])
        } else {
            self.collectionView.reloadItemsAtIndexPaths([indexPath])
        }
    }
    
    private func placeCityPin(city :TXCity) {
        if let annotation = self.annotation {
            self.mapView.removeAnnotation(annotation)
        }
        
        if let location = city.location {
            self.annotation = MKPointAnnotation()
            self.annotation.coordinate = location.coordinate
            self.annotation.title = city.cityName
            
            self.mapView.addAnnotation(self.annotation)
            
            
            let region = self.mapView.regionThatFits(MKCoordinateRegionMakeWithDistance(location.coordinate, 10000, 10000))
            self.mapView.setRegion(region, animated: true)
        }
    }
}

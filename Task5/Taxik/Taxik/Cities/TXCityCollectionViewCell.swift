//
//  TXCityCollectionViewCell.swift
//  Taxik
//
//  Created by Oleg Badretdinov on 24.05.16.
//  Copyright © 2016 Test. All rights reserved.
//

import UIKit

class TXCityCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var coordinatesLabel: UILabel!
    
    override func prepareForReuse() {
        self.backgroundColor = UIColor(white: 0.0, alpha: 0.10)
        
        self.nameLabel.text = ""
        self.coordinatesLabel.text = ""
    }
}

//
//  TaxikAssembly.swift
//  Taxik
//
//  Created by Oleg Badretdinov on 22.05.16.
//  Copyright © 2016 Test. All rights reserved.
//
import Typhoon
import TaxikApi
import AFNetworking

private let kBaseUrl = NSURL(string:"http://beta.taxistock.ru")

class TaxikAssembly: TyphoonAssembly {
    
    dynamic func apiNetworkModel() -> AnyObject {
        return TyphoonDefinition.withClass(TXNetworkModel.self) { (defenition) in
            defenition.useInitializer(#selector(TXNetworkModel.init(httpClient:))) { (initializer) in
                initializer.injectParameterWith(self.sessionManager())
            }
        }
    }

    dynamic func txCitiesViewModel() -> AnyObject {
        return TyphoonDefinition.withClass(TXCitiesViewModel.self) {
            (definition) in
            
            definition.useInitializer(#selector(TXCitiesViewModel.init(networkModel:))) {
                (initializer) in
                initializer.injectParameterWith(self.apiNetworkModel())
            }
            
            definition.scope = TyphoonScope.Singleton
        }
    }
    
    dynamic func sessionManager() -> AnyObject {
        return TyphoonDefinition.withClass(AFHTTPSessionManager.self) { (defenition) in
            defenition.useInitializer(#selector(AFHTTPSessionManager.init(baseURL:))) {
                (initializer) in
                initializer.injectParameterWith(kBaseUrl!)
            }
            
            let serializer = AFJSONResponseSerializer()
            serializer.acceptableContentTypes = ["application/json", "text/json", "text/javascript","text/html"]
            defenition.injectProperty(Selector("responseSerializer"), with: serializer)
            
            defenition.scope = TyphoonScope.Singleton
        }
    }
    
}

//
//  UIKitExtensions.swift
//  ReactiveForms
//
//  Created by Rhys Powell on 12/10/2015.
//  Copyright © 2015 Rhys Powell. All rights reserved.
//

import Foundation
import ReactiveCocoa

func lazyAssociatedProperty<T: AnyObject>(host: AnyObject,
    key: UnsafePointer<Void>, factory: ()->T) -> T {
        var associatedProperty = objc_getAssociatedObject(host, key) as? T
        
        if associatedProperty == nil {
            associatedProperty = factory()
            objc_setAssociatedObject(host, key, associatedProperty,
                .OBJC_ASSOCIATION_RETAIN)
        }
        return associatedProperty!
}

func lazyMutableProperty<T>(host: AnyObject, key: UnsafePointer<Void>,
    setter: T -> (), getter: () -> T) -> MutableProperty<T> {
        return lazyAssociatedProperty(host, key: key) {
            let property = MutableProperty<T>(getter())
            property.producer
                .startWithNext { setter($0) }
            return property
        }
}

struct AssociationKey {
    static var hidden: UInt8 = 1
    static var alpha: UInt8 = 2
    static var text: UInt8 = 3
    static var enabled: UInt8 = 4
    static var action: UInt8 = 5
}

extension UIView {
    public var rac_hidden: MutableProperty<Bool> {
        return lazyMutableProperty(self, key: &AssociationKey.hidden, setter: { self.hidden = $0 }, getter: { return self.hidden })
    }
    
    public var rac_alpha: MutableProperty<CGFloat> {
        return lazyMutableProperty(self, key: &AssociationKey.alpha, setter: { self.alpha = $0 }, getter: { return self.alpha })
    }
}